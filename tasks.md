
1. implement Zero-Copy dataset classes for XGC
  a. Implement ArrayHandleXGCPointCoordinates [d]
  b. Implement CellSetToroid [d]
  c. Implement ConnectivityToroid [d]
  d. Implement ReverseConnectivityToroid [d]
  f. add reverse connectivity tests [d]

2. setup a library for xgc-vtkm
  - build in the control side components for xgc dataset [d]
  - build in the exec side components for xgc ( excluding cuda )[d]
  - verify these work with cuda [d]

3. setup install rules for xgc-vtkm [d]

4. Optimize fetches for the point coordinates when being used with
   XGCConnectivity [d]

6. Setup a filter policy for xgc 

7. build a collection of common filters with the xgc policy into the library
