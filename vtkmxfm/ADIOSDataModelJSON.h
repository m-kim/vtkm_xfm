//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  Under the terms of Contract ???? with DoE,
//  the U.S. Government retains certain rights in this software.
//
//============================================================================
#ifndef ADIOSDataModelJSON_H
#define ADIOSDataModelJSON_H

#include <sstream>
#include <stdexcept>

#include <rapidjson/document.h>

#include "ADIOSDataModel.h"

namespace adios2
{
namespace datamodel
{

#define ThrowJSONUnsupportedTypeError(typeDescr, json) \
{ throw std::runtime_error("Unsupported " + std::string(typeDescr ) + " : " + std::string(json.GetString())); }

template<typename ValueType>
DataSourceMode JSONToDataSourceMode(const ValueType& json)
{
  if(json == "multi_step")
  {
    return DataSourceMode::MultiStep;
  }
  if(json == "single_step")
  {
    return DataSourceMode::SingleStep;
  }
  if(json == "series")
  {
    return DataSourceMode::Series;
  }
  ThrowJSONUnsupportedTypeError("datasource mode", json);
}

template<typename ValueType>
DataSource JSONToDataSource(const ValueType& json)
{
  DataSource source;
  source.Source = json["source"].GetString();
  source.Mode = JSONToDataSourceMode(json["mode"]);
  if(source.Mode == DataSourceMode::Series)
  {
    source.SeriesFormat = json["series_format"].GetString();
  }
  return source;
}

template<typename ValueType>
CoordinateSystemType JSONToCoordinateSystemType(const ValueType& json)
{
  if(json == "cartesian")
  {
    return CoordinateSystemType::Cartesian;
  }
  if(json == "polar")
  {
    return CoordinateSystemType::Polar;
  }
  if(json == "cylindrical")
  {
    return CoordinateSystemType::Cylindrical;
  }
  if(json == "spherical")
  {
    return CoordinateSystemType::Spherical;
  }
  if(json == "magnetic_rzt")
  {
    return CoordinateSystemType::MagneticRZT;
  }
  ThrowJSONUnsupportedTypeError("coordinate system type", json);
}

template<typename ValueType>
CoordinateSystemDimensions JSONToCoordinateSystemDimensions(
  const ValueType& json)
{
  if(json.IsInt())
  {
    switch(json.GetInt())
    {
      case 1: return CoordinateSystemDimensions::OneD;
      case 2: return CoordinateSystemDimensions::TwoD;
      case 3: return CoordinateSystemDimensions::ThreeD;
      default: break;
    }
  }
  else if(json.IsFloat() && json.GetFloat() == 2.5f)
  {
    return CoordinateSystemDimensions::TwoAndHalfD;
  }
  ThrowJSONUnsupportedTypeError("coordinate system dimensions", json);
}

template<typename ValueType>
CoordinateSystem JSONToCoordinateSystem(const ValueType& json)
{
  CoordinateSystem coord;
  coord.System = JSONToCoordinateSystemType(json["system"]);
  coord.Dims = JSONToCoordinateSystemDimensions(json["dims"]);
  if(coord.Dims == CoordinateSystemDimensions::TwoAndHalfD)
  {
    coord.DimPlanar = json["dim_planar"].GetString();
    if(json["num_planes"].IsInt())
    {
      coord.NumPlanes = json["num_planes"].GetInt();
    }
    else
    {
      coord.NumPlanesScalar = json["num_planes"].GetString();
    }
  }
  return coord;
}

template<typename ValueType>
PointArrayLayout JSONToPointArrayLayout(const ValueType &json)
{
  if(json == "aos")
  {
    return PointArrayLayout::AOS;
  }
  if(json == "soa")
  {
    return PointArrayLayout::SOA;
  }
  //if(json == "seperate")
  //{
  //  return PointArrayLayout::Seperate;
  //}
  ThrowJSONUnsupportedTypeError("point array layout", json);
}

template<typename ValueType>
void JSONToDataModelBase(DataModelBase& base, const ValueType &json)
{
  if(json.HasMember("name"))
  {
    base.Name = json["name"].GetString();
  }
  if(json.HasMember("data_source"))
  {
    base.Source = std::make_shared<DataSource>(
      JSONToDataSource(json["data_source"]));
  }
  if(json.HasMember("static"))
  {
    base.Static = json["static"].GetBool();
  }
}

template<typename ValueType>
PointArray JSONToPointArray(const ValueType &json)
{
  PointArray points;

  JSONToDataModelBase(points, json);

  points.Coordinates = JSONToCoordinateSystem(json["coordinates"]);
  points.Layout = JSONToPointArrayLayout(json["layout"]);
  points.Array = json["data"].GetString();

  return points;
}

template<typename ValueType>
CellShape JSONToCellShape(const ValueType &json)
{
  if(json == "vertex")
  {
    return CellShape::Vertex;
  }
  if(json == "line")
  {
    return CellShape::Line;
  }
  if(json == "triangle")
  {
    return CellShape::Triangle;
  }
  if(json == "polygon")
  {
    return CellShape::Polygon;
  }
  if(json == "quad")
  {
    return CellShape::Quad;
  }
  if(json == "tetrahedron")
  {
    return CellShape::Tetrahedron;
  }
  if(json == "hexahedron")
  {
    return CellShape::Hexahedron;
  }
  if(json == "wedge")
  {
    return CellShape::Wedge;
  }
  if(json == "pyramid")
  {
    return CellShape::Pyramid;
  }
  ThrowJSONUnsupportedTypeError("cell shape", json);
}


template<typename ValueType>
CellArrayEncoding JSONToCellArrayEncoding(const ValueType &json)
{
  if(json == "constant")
  {
    return CellArrayEncoding::Constant;
  }
  if(json == "inplace")
  {
    return CellArrayEncoding::InPlace;
  }
  if(json == "array")
  {
    return CellArrayEncoding::Array;
  }
  ThrowJSONUnsupportedTypeError("cell array encoding", json);
}

template<typename ValueType>
CellArray JSONToCellArray(const ValueType &json)
{
  CellArray cells;

  JSONToDataModelBase(cells, json);

  cells.Array = json["data"].GetString();
  if(json.HasMember("encoding"))
  {
    cells.Encoding = JSONToCellArrayEncoding(json["encoding"]);
    if(cells.Encoding == CellArrayEncoding::Constant)
    {
      if(json.HasMember("shape"))
      {
        cells.ShapeConstant = JSONToCellShape(json["shape"]);
      }
    }
    else if(cells.Encoding == CellArrayEncoding::Array)
    {
      cells.ShapeArray = json["shape"].GetString();
    }
  }
  return cells;
};

template<typename ValueType>
CellSetShape JSONToCellSetShape(const ValueType &json)
{
  if(json == "unstructured")
  {
    return CellSetShape::Unstructured;
  }
  if(json == "toroid")
  {
    return CellSetShape::Toroid;
  }
  ThrowJSONUnsupportedTypeError("cell_set shape", json);
}

template<typename ValueType>
CellSet JSONToCellSet(const ValueType &json)
{
  CellSet cellset;

  JSONToDataModelBase(cellset, json);

  cellset.Shape = JSONToCellSetShape(json["shape"]);
  switch(cellset.Shape)
  {
    case CellSetShape::Unstructured:
    case CellSetShape::Toroid:
      cellset.SetPoints(JSONToPointArray(json["points"]));
      cellset.SetCells(JSONToCellArray(json["cells"]));
      break;
    default: break;
  }

  return cellset;
}

template<typename ValueType>
FieldAssociationType JSONToFieldAssociationType(const ValueType &json)
{
  if(json == "any")
  {
    return FieldAssociationType::Any;
  }
  if(json == "whole_mesh")
  {
    return FieldAssociationType::WholeMesh;
  }
  if(json == "points")
  {
    return FieldAssociationType::Points;
  }
  if(json == "cell_set")
  {
    return FieldAssociationType::CellSet;
  }
  ThrowJSONUnsupportedTypeError("field association", json);
}

template<typename ValueType>
FieldAssociation JSONToFieldAssociation(const ValueType &json)
{
  FieldAssociation association;
  association.AssociationType = JSONToFieldAssociationType(
    json["association_type"]);
  switch(association.AssociationType)
  {
    case FieldAssociationType::Points:
    case FieldAssociationType::CellSet:
      association.AssociationLabel = json["association_label"].GetString();
      break;
    default: break;
  }

  return association;
}


template<typename ValueType>
Field JSONToField(const ValueType &json)
{
  Field field;

  JSONToDataModelBase(field, json);

  field.Association = JSONToFieldAssociation(json["association"]);
  field.Array = json["data"].GetString();

  return field;
}

template<typename ValueType>
DataSet JSONToDataSet(const ValueType &json)
{
  DataSet dataset;

  JSONToDataModelBase(dataset, json);

  for(auto &jsonCellSet: json["cell_sets"].GetArray())
  {
    dataset.AddCellSet(JSONToCellSet(jsonCellSet));
  }
  for(auto &jsonField: json["fields"].GetArray())
  {
    dataset.AddField(JSONToField(jsonField));
  }

  return dataset;
}

} // end namespace datamodel
} // end namespace adios2

#endif // ADIOSDataModelJSON_H
