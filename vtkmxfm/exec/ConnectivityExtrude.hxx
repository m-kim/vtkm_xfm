
#include "ConnectivityExtrude.h"

namespace vtkm
{
namespace exec
{

template <typename Device>
ConnectivityExtrude<Device>::ConnectivityExtrude(const ConnectivityPortalType& conn,
                                         const ConnectivityPortalType& nextNode,
                                         vtkm::Int32 cellsPerPlane,
                                         vtkm::Int32 pointsPerPlane,
                                         vtkm::Int32 numPlanes,
                                         bool periodic)
  : Connectivity(conn)
  , NextNode(nextNode)
  , NumberOfCellsPerPlane(cellsPerPlane)
  , NumberOfPointsPerPlane(pointsPerPlane)
  , NumberOfPlanes(numPlanes)
{
  this->NumberOfCells = periodic ? (static_cast<vtkm::Id>(cellsPerPlane) * numPlanes) :
                                   (static_cast<vtkm::Id>(cellsPerPlane) * (numPlanes - 1));
}

template <typename Device>
typename ConnectivityExtrude<Device>::IndicesType
ConnectivityExtrude<Device>::GetIndices(const vtkm::Id2& index) const
{
  vtkm::Id tr = index[0];
  vtkm::Id p0 = index[1];
  vtkm::Id p1 = (p0 < (this->NumberOfPlanes - 1)) ? (p0 + 1) : 0;

  vtkm::Vec<vtkm::Int32, 3> pointIds1, pointIds2;
  for (int i = 0; i < 3; ++i)
  {
    pointIds1[i] = this->Connectivity.Get((tr * 3) + i);
    pointIds2[i] = this->NextNode.Get(pointIds1[i]);
  }

  return IndicesType(pointIds1, p0, pointIds2, p1, this->NumberOfPointsPerPlane);
}


template <typename Device>
ReverseConnectivityExtrude<Device>::ReverseConnectivityExtrude(
  const ConnectivityPortalType& conn,
  const OffsetsPortalType& offsets,
  const CountsPortalType& counts,
  const PrevNodePortalType& prevNode,
  vtkm::Int32 cellsPerPlane,
  vtkm::Int32 pointsPerPlane,
  vtkm::Int32 numPlanes)
  : Connectivity(conn)
  , Offsets(offsets)
  , Counts(counts)
  , PrevNode(prevNode)
  , NumberOfCellsPerPlane(cellsPerPlane)
  , NumberOfPointsPerPlane(pointsPerPlane)
  , NumberOfPlanes(numPlanes)
{
}

template <typename Device>
typename ReverseConnectivityExtrude<Device>::IndicesType
ReverseConnectivityExtrude<Device>::GetIndices(const vtkm::Id2& index) const
{
  auto ptCur = index[0];
  auto ptPre = this->PrevNode.Get(ptCur);
  auto plCur = index[1];
  auto plPre = (plCur == 0) ? (this->NumberOfPlanes - 1) : (plCur - 1);

  return IndicesType(this->Connectivity,
                     this->Offsets.Get(ptPre),
                     this->Counts.Get(ptPre),
                     this->Offsets.Get(ptCur),
                     this->Counts.Get(ptCur),
                     plPre,
                     plCur,
                     this->NumberOfCellsPerPlane);
}

}
} // vtkm::exec
