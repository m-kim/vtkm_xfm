#ifndef vtk_m_exec_ConnectivityExtrude_h
#define vtk_m_exec_ConnectivityExtrude_h

#include "vtkmxfm/vtkm_xfm_export.h"
#include <vtkmxfm/internal/IndicesExtrude.h>

#include <vtkm/CellShape.h>
#include <vtkm/cont/ArrayHandle.h>

#include <vtkm/exec/arg/ThreadIndicesTopologyMap.h>


namespace vtkm
{
namespace exec
{

template <typename Device>
class VTKM_ALWAYS_EXPORT ConnectivityExtrude
{
private:
  using Int32HandleType = vtkm::cont::ArrayHandle<vtkm::Int32>;
  using Int32PortalType =
    typename Int32HandleType::template ExecutionTypes<Device>::PortalConst;

public:
  using ConnectivityPortalType = Int32PortalType;
  using NextNodePortalType = Int32PortalType;

  using SchedulingRangeType = vtkm::Id2;

  using CellShapeTag = vtkm::CellShapeTagWedge;

  using IndicesType = IndicesExtrude;

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC_CONT
  ConnectivityExtrude() = default;

  ConnectivityExtrude(const ConnectivityPortalType& conn,
                  const NextNodePortalType& nextnode,
                  vtkm::Int32 cellsPerPlane,
                  vtkm::Int32 pointsPerPlane,
                  vtkm::Int32 numPlanes,
                  bool periodic);

  VTKM_EXEC
  vtkm::Id GetNumberOfElements() const
  {
    return this->NumberOfCells;
  }

  VTKM_EXEC
  CellShapeTag GetCellShape(vtkm::Id) const
  {
    return CellShapeTag();
  }

  VTKM_EXEC
  IndicesType GetIndices(vtkm::Id index) const
  {
    return this->GetIndices(this->FlatToLogicalToIndex(index));
  }

  VTKM_EXEC
  IndicesType GetIndices(const vtkm::Id2& index) const;

  VTKM_EXEC
  vtkm::Id LogicalToFlatToIndex(const vtkm::Id2& index) const
  {
    return index[0] + (index[1] * this->NumberOfCellsPerPlane);
  };

  VTKM_EXEC
  vtkm::Id2 FlatToLogicalToIndex(vtkm::Id index) const
  {
    const vtkm::Id cellId = index % this->NumberOfCellsPerPlane;
    const vtkm::Id plane = index / this->NumberOfCellsPerPlane;
    return vtkm::Id2(cellId, plane);
  }

private:
  ConnectivityPortalType Connectivity;
  NextNodePortalType NextNode;
  vtkm::Int32 NumberOfCellsPerPlane;
  vtkm::Int32 NumberOfPointsPerPlane;
  vtkm::Int32 NumberOfPlanes;
  vtkm::Id NumberOfCells;
};


template <typename Device>
class VTKM_ALWAYS_EXPORT ReverseConnectivityExtrude
{
private:
  using Int32HandleType = vtkm::cont::ArrayHandle<vtkm::Int32>;
  using Int32PortalType =
    typename Int32HandleType::template ExecutionTypes<Device>::PortalConst;

public:
  using ConnectivityPortalType = Int32PortalType;
  using OffsetsPortalType = Int32PortalType;
  using CountsPortalType = Int32PortalType;
  using PrevNodePortalType = Int32PortalType;

  using SchedulingRangeType = vtkm::Id2;

  using CellShapeTag = vtkm::CellShapeTagVertex;

  using IndicesType = ReverseIndicesExtrude<ConnectivityPortalType>;

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC_CONT
  ReverseConnectivityExtrude() = default;

  ReverseConnectivityExtrude(const ConnectivityPortalType& conn,
                         const OffsetsPortalType& offsets,
                         const CountsPortalType& counts,
                         const PrevNodePortalType& prevNode,
                         vtkm::Int32 cellsPerPlane,
                         vtkm::Int32 pointsPerPlane,
                         vtkm::Int32 numPlanes);

  VTKM_EXEC
  vtkm::Id GetNumberOfElements() const
  {
    return this->NumberOfPointsPerPlane * this->NumberOfPlanes;
  }

  VTKM_EXEC
  CellShapeTag GetCellShape(vtkm::Id) const
  {
    return vtkm::CellShapeTagVertex();
  }

  /// Returns a Vec-like object containing the indices for the given index.
  /// The object returned is not an actual array, but rather an object that
  /// loads the indices lazily out of the connectivity array. This prevents
  /// us from having to know the number of indices at compile time.
  ///
  VTKM_EXEC
  IndicesType GetIndices(vtkm::Id index) const
  {
    return this->GetIndices(this->FlatToLogicalToIndex(index));
  }

  VTKM_EXEC
  IndicesType GetIndices(const vtkm::Id2& index) const;

  VTKM_EXEC
  vtkm::Id LogicalToFlatToIndex(const vtkm::Id2& index) const
  {
    return index[0] + (index[1] * this->NumberOfPointsPerPlane);
  };

  VTKM_EXEC
  vtkm::Id2 FlatToLogicalToIndex(vtkm::Id index) const
  {
    const vtkm::Id vertId = index % this->NumberOfPointsPerPlane;
    const vtkm::Id plane = index / this->NumberOfPointsPerPlane;
    return vtkm::Id2(vertId, plane);
  }

  ConnectivityPortalType Connectivity;
  OffsetsPortalType Offsets;
  CountsPortalType Counts;
  PrevNodePortalType PrevNode;
  vtkm::Int32 NumberOfCellsPerPlane;
  vtkm::Int32 NumberOfPointsPerPlane;
  vtkm::Int32 NumberOfPlanes;
};

}
} // namespace vtkm::exec

#include <vtkm/cont/serial/DeviceAdapterSerial.h>
#include <vtkm/cont/tbb/DeviceAdapterTBB.h>
// template classes we want to compile only once
namespace vtkm
{
namespace exec
{
extern template struct VTKM_XFM_TEMPLATE_EXPORT
  ConnectivityExtrude<vtkm::cont::DeviceAdapterTagSerial>;
extern template struct VTKM_XFM_TEMPLATE_EXPORT
  ReverseConnectivityExtrude<vtkm::cont::DeviceAdapterTagSerial>;
#ifdef VTKM_ENABLE_TBB
extern template struct VTKM_XFM_TEMPLATE_EXPORT
  ConnectivityExtrude<vtkm::cont::DeviceAdapterTagTBB>;
extern template struct VTKM_XFM_TEMPLATE_EXPORT
  ReverseConnectivityExtrude<vtkm::cont::DeviceAdapterTagTBB>;
#endif
}
}
#endif
