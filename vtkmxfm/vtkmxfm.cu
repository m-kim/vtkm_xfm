#include "vtkmxfm/vtkm_xfm_export.h"
#include <vtkmxfm/cont/ArrayHandleExtrudeCoords.h>
#include <vtkmxfm/cont/CellSetExtrude.hxx>
#include <vtkmxfm/exec/ConnectivityExtrude.hxx>
#include <vtkmxfm/internal/ArrayPortalExtrude.h>

#include <vtkm/cont/cuda/DeviceAdapterCuda.h>

namespace vtkm { namespace exec{
template struct VTKM_XFM_TEMPLATE_EXPORT
  ArrayPortalExtrude<cont::internal::ArrayPortalFromIterators<float const*, void>>;
template struct VTKM_XFM_TEMPLATE_EXPORT
  ArrayPortalExtrude<cont::internal::ArrayPortalFromIterators<double const*, void>>;

  template struct VTKM_XFM_TEMPLATE_EXPORT ConnectivityExtrude<vtkm::cont::DeviceAdapterTagCuda>;
  template struct VTKM_XFM_TEMPLATE_EXPORT ReverseConnectivityExtrude<vtkm::cont::DeviceAdapterTagCuda>;

}
}

//exports for the ArrayHandleExtrudeCoords
namespace vtkm { namespace cont {
template class VTKM_XFM_TEMPLATE_EXPORT ArrayHandleExtrudeCoords<float>;
template class VTKM_XFM_TEMPLATE_EXPORT ArrayHandleExtrudeCoords<double>;
} }

//exports for the ArrayHandle<T, StorageTagPointsXGC> internal implementation
namespace vtkm{ namespace cont { namespace internal {
template class VTKM_XFM_TEMPLATE_EXPORT Storage<vtkm::Vec<float, 3>, StorageTagExtrude>;
template class VTKM_XFM_TEMPLATE_EXPORT Storage<vtkm::Vec<double, 3>, StorageTagExtrude>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayHandleExecutionManagerBase<vtkm::Vec<float, 3>, StorageTagExtrude>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayHandleExecutionManagerBase<vtkm::Vec<double, 3>, StorageTagExtrude>;

template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayTransfer<vtkm::Vec<float, 3>, StorageTagExtrude, DeviceAdapterTagCuda>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayTransfer<vtkm::Vec<double, 3>, StorageTagExtrude, DeviceAdapterTagCuda>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayHandleExecutionManager<vtkm::Vec<float, 3>, StorageTagExtrude, DeviceAdapterTagCuda>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayHandleExecutionManager<vtkm::Vec<double, 3>, StorageTagExtrude, DeviceAdapterTagCuda>;

}
}
}

//exports for the ConnectivityXGC
namespace vtkm { namespace cont {
template VTKM_XFM_TEMPLATE_EXPORT CellSetExtrude::ConnectivityP2C<DeviceAdapterTagCuda>
  CellSetExtrude::PrepareForInput(vtkm::cont::DeviceAdapterTagCuda,
                              vtkm::TopologyElementTagPoint,
                              vtkm::TopologyElementTagCell) const;
template VTKM_XFM_TEMPLATE_EXPORT CellSetExtrude::ConnectivityC2P<DeviceAdapterTagCuda>
  CellSetExtrude::PrepareForInput(vtkm::cont::DeviceAdapterTagCuda,
                              vtkm::TopologyElementTagCell,
                              vtkm::TopologyElementTagPoint) const;
}
}


