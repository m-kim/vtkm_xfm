

#include "vtkmxfm/vtkm_xfm_export.h"
#include <vtkmxfm/cont/ArrayHandleExtrudeCoords.h>
#include <vtkmxfm/cont/CellSetExtrude.hxx>
#include <vtkmxfm/exec/ConnectivityExtrude.hxx>
#include <vtkmxfm/internal/ArrayPortalExtrude.hxx>

#include <vtkm/cont/serial/DeviceAdapterSerial.h>
#include <vtkm/cont/tbb/DeviceAdapterTBB.h>


#if defined(VTKM_GCC)
# pragma GCC diagnostic push
# pragma GCC diagnostic ignored "-Wattributes"
#endif

//exports for the ArrayPortalExtrude and Connectivity
namespace vtkm
{
namespace exec
{
template struct VTKM_XFM_TEMPLATE_EXPORT
  ArrayPortalExtrude<cont::internal::ArrayPortalFromIterators<float const*, void>>;
template struct VTKM_XFM_TEMPLATE_EXPORT
  ArrayPortalExtrude<cont::internal::ArrayPortalFromIterators<double const*, void>>;

template struct VTKM_XFM_TEMPLATE_EXPORT ConnectivityExtrude<vtkm::cont::DeviceAdapterTagSerial>;
template struct VTKM_XFM_TEMPLATE_EXPORT ReverseConnectivityExtrude<vtkm::cont::DeviceAdapterTagSerial>;

#ifdef VTKM_ENABLE_TBB
template struct VTKM_XFM_TEMPLATE_EXPORT ConnectivityExtrude<vtkm::cont::DeviceAdapterTagTBB>;
template struct VTKM_XFM_TEMPLATE_EXPORT ReverseConnectivityExtrude<vtkm::cont::DeviceAdapterTagTBB>;
#endif

}
}

//exports for the ArrayHandleExtrudeCoords
namespace vtkm { namespace cont {
template class VTKM_XFM_TEMPLATE_EXPORT ArrayHandleExtrudeCoords<float>;
template class VTKM_XFM_TEMPLATE_EXPORT ArrayHandleExtrudeCoords<double>;
} }

//exports for the ArrayHandle<T, StorageTagPointsXGC> internal implementation
namespace vtkm{ namespace cont { namespace internal {
template class VTKM_XFM_TEMPLATE_EXPORT Storage<vtkm::Vec<float, 3>, StorageTagExtrude>;
template class VTKM_XFM_TEMPLATE_EXPORT Storage<vtkm::Vec<double, 3>, StorageTagExtrude>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayHandleExecutionManagerBase<vtkm::Vec<float, 3>, StorageTagExtrude>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayHandleExecutionManagerBase<vtkm::Vec<double, 3>, StorageTagExtrude>;

template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayTransfer<vtkm::Vec<float, 3>, StorageTagExtrude, DeviceAdapterTagSerial>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayTransfer<vtkm::Vec<double, 3>, StorageTagExtrude, DeviceAdapterTagSerial>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayHandleExecutionManager<vtkm::Vec<float, 3>, StorageTagExtrude, DeviceAdapterTagSerial>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayHandleExecutionManager<vtkm::Vec<double, 3>, StorageTagExtrude, DeviceAdapterTagSerial>;

#ifdef VTKM_ENABLE_TBB
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayTransfer<vtkm::Vec<float, 3>, StorageTagExtrude, DeviceAdapterTagTBB>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayTransfer<vtkm::Vec<double, 3>, StorageTagExtrude, DeviceAdapterTagTBB>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayHandleExecutionManager<vtkm::Vec<float, 3>, StorageTagExtrude, DeviceAdapterTagTBB>;
template class VTKM_XFM_TEMPLATE_EXPORT
  ArrayHandleExecutionManager<vtkm::Vec<double, 3>, StorageTagExtrude, DeviceAdapterTagTBB>;
#endif
}
}
}

//exports for the ConnectivityExtrude
namespace vtkm { namespace cont {
template VTKM_XFM_TEMPLATE_EXPORT CellSetExtrude::ConnectivityP2C<DeviceAdapterTagSerial>
  CellSetExtrude::PrepareForInput(vtkm::cont::DeviceAdapterTagSerial,
                              vtkm::TopologyElementTagPoint,
                              vtkm::TopologyElementTagCell) const;
template VTKM_XFM_TEMPLATE_EXPORT CellSetExtrude::ConnectivityC2P<DeviceAdapterTagSerial>
  CellSetExtrude::PrepareForInput(vtkm::cont::DeviceAdapterTagSerial,
                              vtkm::TopologyElementTagCell,
                              vtkm::TopologyElementTagPoint) const;

#ifdef VTKM_ENABLE_TBB
template VTKM_XFM_TEMPLATE_EXPORT CellSetExtrude::ConnectivityP2C<DeviceAdapterTagTBB>
  CellSetExtrude::PrepareForInput(vtkm::cont::DeviceAdapterTagTBB,
                              vtkm::TopologyElementTagPoint,
                              vtkm::TopologyElementTagCell) const;
template VTKM_XFM_TEMPLATE_EXPORT CellSetExtrude::ConnectivityC2P<DeviceAdapterTagTBB>
  CellSetExtrude::PrepareForInput(vtkm::cont::DeviceAdapterTagTBB,
                              vtkm::TopologyElementTagCell,
                              vtkm::TopologyElementTagPoint) const;
#endif
}
}

#if defined(VTKM_GCC)
# pragma GCC diagnostic pop
#endif
