//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  Under the terms of Contract DE-NA0003525 with NTESS,
//  the U.S. Government retains certain rights in this software.
//
//============================================================================
#ifndef vtk_m_io_reader_ADIOS1DataSetReader_h
#define vtk_m_io_reader_ADIOS1DataSetReader_h

#include <memory>
#include <string>

#include <vtkm/cont/DataSet.h>

#include "vtkmxfm/vtkm_xfm_export.h"

namespace vtkm
{
namespace io
{
namespace reader
{

class VTKM_XFM_EXPORT ADIOS1DataSetReader
{
public:
  ADIOS1DataSetReader(const std::string dataModelFilename,
                      const std::string dataModelType,
                      bool genCartesian);

  virtual ~ADIOS1DataSetReader();

  const vtkm::cont::DataSet& ReadDataSet();

  int GetStep() const;
  bool Advance();

private:
  class ADIOS1DataSetReaderImpl;
  std::unique_ptr<ADIOS1DataSetReaderImpl> Impl;
  bool GenCartesian;
};

} // end namespace reader
} // end namespace io
} // end namespace vtkm

#endif // vtk_m_io_reader_ADIOS1DataSetReader_h

