#ifndef _ADIOSDataModel_H_
#define _ADIOSDataModel_H_

#include <memory>
#include <string>
#include <vector>

namespace adios2
{
namespace datamodel
{

enum class DataSourceMode
{
  SingleStep,
  MultiStep,
  Series
};

struct DataSource
{
  std::string Source;
  DataSourceMode Mode;
  std::string SeriesFormat;
};

enum class CoordinateSystemType
{
  Cartesian,
  Polar,
  Cylindrical,
  Spherical,
  MagneticRZT
};

enum class CoordinateSystemDimensions
{
  OneD,
  TwoD,
  TwoAndHalfD,
  ThreeD
};

struct CoordinateSystem
{
  CoordinateSystemType System = CoordinateSystemType::Cartesian;
  CoordinateSystemDimensions Dims = CoordinateSystemDimensions::ThreeD;
  std::string DimPlanar;
  std::string NumPlanesScalar;
  size_t NumPlanes;
};

enum class PointArrayLayout
{
  AOS,
  SOA,
  Seperate
};

enum class CellSetShape
{
  Unstructured,
  Toroid
};

struct DataModelBase
{
  DataModelBase() = default;
  DataModelBase(const DataModelBase &other)
  {
    if(this != &other)
    {
      this->Source = other.Source;
      this->Name = other.Name;
      this->Static = other.Static;
    }
  }

  std::shared_ptr<const DataSource> Source = nullptr;
  std::string Name = "";
  bool Static = false;
};

struct PointArray: public DataModelBase
{
  CoordinateSystem Coordinates;
  PointArrayLayout Layout = PointArrayLayout::AOS;
  std::string Array;
};

enum class CellShape
{
  Vertex,
  Line,
  Triangle,
  Polygon,
  Quad,
  Tetrahedron,
  Hexahedron,
  Wedge,
  Pyramid
};

enum class CellArrayEncoding
{
  Constant,
  InPlace,
  Array
};

struct CellArray: public DataModelBase
{
  std::string Array;
  CellArrayEncoding Encoding = CellArrayEncoding::Constant;
  CellShape ShapeConstant = CellShape::Triangle;
  std::string ShapeArray;
};

struct CellSet: public DataModelBase
{
  void SetPoints(PointArray points)
  {
    if(!points.Source)
    {
      points.Source = this->Source;
    }
    this->Points = points;
  }

  void SetCells(CellArray cells)
  {
    if(!cells.Source)
    {
      cells.Source = this->Source;
    }
    this->Cells = cells;
  }

  CellSetShape Shape = CellSetShape::Unstructured;
  PointArray Points;
  CellArray Cells;
};

enum class FieldAssociationType
{
  Any,
  WholeMesh,
  Points,
  CellSet
};

struct FieldAssociation
{
  FieldAssociationType AssociationType = FieldAssociationType::Any;
  std::string AssociationLabel;
};

struct Field: public DataModelBase
{
  std::string Array;
  FieldAssociation Association;
};

struct DataSet: public DataModelBase
{
  void AddCellSet(CellSet cellSet)
  {
    if(!cellSet.Source)
    {
      cellSet.Source = this->Source;
    }
    this->CellSets.push_back(cellSet);
  }

  void AddField(Field field)
  {
    if(!field.Source)
    {
      field.Source = this->Source;
    }
    this->Fields.push_back(field);
  }

  std::vector<CellSet> CellSets;
  std::vector<Field> Fields;
};


} // end namespace datamodel
} // end namespace adios2

#endif
