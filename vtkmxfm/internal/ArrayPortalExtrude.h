
#ifndef vtkmxfm_internal_ArrayPortalExtrude_h
#define vtkmxfm_internal_ArrayPortalExtrude_h

#include "vtkmxfm/vtkm_xfm_export.h"
#include <vtkmxfm/internal/IndicesExtrude.h>

#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/ErrorBadType.h>
#include <vtkm/cont/ErrorInternal.h>

#include <vtkm/BaseComponent.h>
#include <vtkmxfm/cont/StorageExtrude.h>

namespace vtkm
{
namespace exec
{

template <typename PortalType>
struct VTKM_ALWAYS_EXPORT ArrayPortalExtrude
{
  using ValueType = vtkm::Vec<typename PortalType::ValueType, 3>;

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC_CONT
  ArrayPortalExtrude()
    : Portal()
    , NumberOfValues(0)
    , NumberOfPlanes(0)
    , UseCylindrical(false) {};

  ArrayPortalExtrude(const PortalType& p, vtkm::Int32 numOfValues, vtkm::Int32 numOfPlanes,
    bool cylindrical = false)
    : Portal(p)
    , NumberOfValues(numOfValues)
    , NumberOfPlanes(numOfPlanes)
    , UseCylindrical(cylindrical)
  {
  }

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC_CONT
  vtkm::Id GetNumberOfValues() const { return ((NumberOfValues/2) * static_cast<vtkm::Id>(NumberOfPlanes)) ; }

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC_CONT
  ValueType Get(vtkm::Id index) const;

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC_CONT
  ValueType Get(vtkm::Id2 index) const;

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC_CONT
  vtkm::Vec<ValueType,6> GetWedge(const IndicesExtrude& index) const;

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC_CONT
  void Set(vtkm::Id vtkmNotUsed(index), const ValueType& vtkmNotUsed(value)) const {}

  PortalType Portal;
  vtkm::Int32 NumberOfValues;
  vtkm::Int32 NumberOfPlanes;
  bool UseCylindrical;

};

extern template struct VTKM_XFM_TEMPLATE_EXPORT ArrayPortalExtrude<cont::internal::ArrayPortalFromIterators<float const*, void>>;
extern template struct VTKM_XFM_TEMPLATE_EXPORT ArrayPortalExtrude<cont::internal::ArrayPortalFromIterators<double const*, void>>;
} }


#endif
