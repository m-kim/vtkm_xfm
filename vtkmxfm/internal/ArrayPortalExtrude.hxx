
#include <vtkmxfm/internal/ArrayPortalExtrude.h>

#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/ErrorBadType.h>
#include <vtkm/cont/ErrorInternal.h>

#include <vtkm/BaseComponent.h>

namespace vtkm
{
namespace exec
{

template <typename PortalType>
typename ArrayPortalExtrude<PortalType>::ValueType
ArrayPortalExtrude<PortalType>::ArrayPortalExtrude::Get(vtkm::Id index) const
{
  using CompType = typename ValueType::ComponentType;

  const vtkm::Id realIdx = (index * 2) % this->NumberOfValues;
  const vtkm::Id whichPlane = (index * 2) / this->NumberOfValues;
  const auto phi = static_cast<CompType>(whichPlane * (vtkm::TwoPi() / this->NumberOfPlanes));

  auto r = this->Portal.Get(realIdx);
  auto z = this->Portal.Get(realIdx + 1);
  if (this->UseCylindrical)
  {
    return ValueType(r, phi, z);
  }
  else
  {
    return ValueType(r * vtkm::Cos(phi), r * vtkm::Sin(phi), z);
  }
}

template <typename PortalType>
typename ArrayPortalExtrude<PortalType>::ValueType
ArrayPortalExtrude<PortalType>::ArrayPortalExtrude::Get(vtkm::Id2 index) const
{
  using CompType = typename ValueType::ComponentType;

  const vtkm::Id realIdx = (index[0] * 2);
  const vtkm::Id whichPlane = index[1];
  const auto phi = static_cast<CompType>(whichPlane * (vtkm::TwoPi() / this->NumberOfPlanes));

  auto r = this->Portal.Get(realIdx);
  auto z = this->Portal.Get(realIdx + 1);
  if (this->UseCylindrical)
  {
    return ValueType(r, phi, z);
  }
  else
  {
    return ValueType(r * vtkm::Cos(phi), r * vtkm::Sin(phi), z);
  }
}

template <typename PortalType>
vtkm::Vec<typename ArrayPortalExtrude<PortalType>::ValueType,6>
ArrayPortalExtrude<PortalType>::ArrayPortalExtrude::GetWedge(const IndicesExtrude& index) const
{
  using CompType = typename ValueType::ComponentType;

  vtkm::Vec<ValueType, 6> result;
  for (int j = 0; j < 2; ++j)
  {
    const auto phi = static_cast<CompType>(index.Planes[j] * (vtkm::TwoPi() / this->NumberOfPlanes));
    for (int i = 0; i < 3; ++i)
    {
      const vtkm::Id realIdx = index.PointIds[j][i] * 2;
      auto r = this->Portal.Get(realIdx);
      auto z = this->Portal.Get(realIdx + 1);
      result[3*j + i] = this->UseCylindrical ?
                        ValueType(r, phi, z) :
                        ValueType(r * vtkm::Cos(phi), r * vtkm::Sin(phi), z);
    }
  }

  return result;
}

}
}
