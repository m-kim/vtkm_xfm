//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  Under the terms of Contract ???? with DoE,
//  the U.S. Government retains certain rights in this software.
//
//============================================================================

#include "ADIOS1DataSetReader.h"

#include <cassert>
#include <cstdio>

#include <ios>
#include <memory>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>

#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>

#include <adios_read.h>

#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/Field.h>
#include <vtkm/cont/CellSet.h>
#include <vtkm/cont/DataSet.h>

#include <vtkmxfm/cont/ArrayHandleExtrudeCoords.h>
#include <vtkmxfm/cont/CellSetExtrude.h>

#include "adios/ADIOSReader.h"
#include "adios/ADIOSUtilities.h"
#include "ADIOSDataModel.h"
#include "ADIOSDataModelJSON.h"

namespace vtkm
{
namespace io
{
namespace reader
{

class ADIOS1DataSetReader::ADIOS1DataSetReaderImpl
{
public:

  ADIOS1DataSetReaderImpl(const std::string dataModelFileName,
    const std::string dataModelType)
  {
    this->ReadJSONFile(dataModelFileName, dataModelType);
  }

  void ReadJSONFile(const std::string dataModelFileName,
    const std::string dataModelType)
  {
    // Parse the JSON metadata file
    FILE *fp = std::fopen(dataModelFileName.c_str(), "rb");
    if(!fp)
    {
      throw std::ios_base::failure("Unable to open metadata file");
    }

    std::vector<char> buffer(65536);

    rapidjson::FileReadStream is(fp, buffer.data(), buffer.size());

    rapidjson::Document document;
    document.ParseStream(is);
    std::fclose(fp);

    assert(document.IsObject());

    // Polulate the data model structure
    if(!document.HasMember(dataModelType.c_str()))
    {
      throw std::runtime_error("Unsupported datamodel type: " + dataModelType);
    }
    std::cerr << "Loading " << dataModelType << " data model from "
     << dataModelFileName << std::endl;
    this->DataModelDataSet =
      adios2::datamodel::JSONToDataSet(document[dataModelType.c_str()]);
  }

  // Create the ADIOS readers and open the necessary files
  void InitializeADIOSReaders()
  {
    // Build the initial set of readers

    // Get the top level reader
    this->SourceReaders[this->DataModelDataSet.Source] =
      std::make_shared<ADIOS::Reader>();

    // Check for additional readers in cell sets
    for(auto &cellSet: this->DataModelDataSet.CellSets)
    {
      if(this->SourceReaders.count(cellSet.Source) == 0)
      {
        this->SourceReaders[cellSet.Source] =
          std::make_shared<ADIOS::Reader>();
      }
      if(this->SourceReaders.count(cellSet.Points.Source) == 0)
      {
        this->SourceReaders[cellSet.Points.Source] =
          std::make_shared<ADIOS::Reader>();
      }
      this->VariableSources[&cellSet.Points] =
        std::make_pair(cellSet.Points.Array, cellSet.Points.Source);

      if(this->SourceReaders.count(cellSet.Cells.Source) == 0)
      {
        this->SourceReaders[cellSet.Cells.Source] =
          std::make_shared<ADIOS::Reader>();
      }
      this->VariableSources[&cellSet.Cells] =
        std::make_pair(cellSet.Cells.Array, cellSet.Cells.Source);
    }

    // Check for additional readers in the fields
    for(auto &field: this->DataModelDataSet.Fields)
    {
      if(this->SourceReaders.count(field.Source) == 0)
      {
        this->SourceReaders[field.Source] = std::make_shared<ADIOS::Reader>();
      }
      this->VariableSources[&field] = std::make_pair(field.Array, field.Source);
    }

  }

  void BuildVariableInfo()
  {
    for(auto &variableSource: this->VariableSources)
    {
      auto reader = this->SourceReaders[variableSource.second.second];
      for(auto var: reader->GetArrays())
      {
        if(var->GetName() == variableSource.second.first)
        {
          this->VariableInfo[variableSource.first] = var;
          break;
        }
      }
    }
  }


  adios2::datamodel::DataSet DataModelDataSet;
  std::unordered_map<
    std::shared_ptr<const adios2::datamodel::DataSource>,
    std::shared_ptr<ADIOS::Reader>> SourceReaders;
  std::unordered_map<
    const adios2::datamodel::DataModelBase*,
    std::pair<
      std::string,
      std::shared_ptr<const adios2::datamodel::DataSource>>>
    VariableSources;
  std::unordered_map<
    const adios2::datamodel::DataModelBase*,
    const ADIOS::VarInfo*>
    VariableInfo;

  std::unordered_map<std::string, std::vector<char>> ArrayBuffers;

  vtkm::cont::DataSet Output;
  int CurrentStep = -1;
};


ADIOS1DataSetReader::ADIOS1DataSetReader(const std::string dataModelFileName,
  const std::string dataModelType, bool genCartesian)
: Impl(new ADIOS1DataSetReaderImpl(dataModelFileName, dataModelType))
, GenCartesian(genCartesian)
{
}


ADIOS1DataSetReader::~ADIOS1DataSetReader()
{
}


inline
int DimsSize(const std::vector<size_t> &dims)
{
  return std::accumulate(dims.begin(), dims.end(), 1, std::multiplies<int>());
}

template <typename Functor, typename... Args>
inline void CastBufferAndCall(void *buf, ADIOS_DATATYPES type, Functor&& calee, Args&&... args)
{
  switch (type)
  {
    case adios_byte:
      calee(reinterpret_cast<int8_t*>(buf), std::forward<Args>(args)...);
      break;
    case adios_short:
      calee(reinterpret_cast<int16_t*>(buf), std::forward<Args>(args)...);
      break;
    case adios_integer:
      calee(reinterpret_cast<int32_t*>(buf), std::forward<Args>(args)...);
      break;
    case adios_long:
      calee(reinterpret_cast<int64_t*>(buf), std::forward<Args>(args)...);
      break;
    case adios_unsigned_byte:
      calee(reinterpret_cast<uint8_t*>(buf), std::forward<Args>(args)...);
      break;
    case adios_unsigned_short:
      calee(reinterpret_cast<uint16_t*>(buf), std::forward<Args>(args)...);
      break;
    case adios_unsigned_integer:
      calee(reinterpret_cast<uint32_t*>(buf), std::forward<Args>(args)...);
      break;
    case adios_unsigned_long:
      calee(reinterpret_cast<uint64_t*>(buf), std::forward<Args>(args)...);
      break;
    case adios_real:
      calee(reinterpret_cast<float*>(buf), std::forward<Args>(args)...);
      break;
    case adios_double:
      calee(reinterpret_cast<double*>(buf), std::forward<Args>(args)...);
      break;
    case adios_complex:
      calee(reinterpret_cast<vtkm::Vec<float, 2>*>(buf), std::forward<Args>(args)...);
      break;
    case adios_double_complex:
      calee(reinterpret_cast<vtkm::Vec<double, 2>*>(buf), std::forward<Args>(args)...);
      break;
    default:
      VTKM_ASSERT(false);
      break;
  }
}

inline vtkm::cont::Field::Association FieldAssociationAdiosToVTKm(
  adios2::datamodel::FieldAssociationType type)
{
  switch(type)
  {
    case adios2::datamodel::FieldAssociationType::Points:
      return vtkm::cont::Field::Association::POINTS;
    case adios2::datamodel::FieldAssociationType::CellSet:
      return vtkm::cont::Field::Association::CELL_SET;
    case adios2::datamodel::FieldAssociationType::WholeMesh:
      return vtkm::cont::Field::Association::WHOLE_MESH;
    default:
      return vtkm::cont::Field::Association::ANY;
  }
}

struct BuildFieldFunctor
{
  template <typename T>
  void operator()(T* buf,
                  vtkm::Id size,
                  const std::string& name,
                  vtkm::cont::Field::Association assoc,
                  const std::string assocLabel,
                  vtkm::cont::Field& field) const
  {
    switch (assoc)
    {
      case vtkm::cont::Field::Association::POINTS:
      case vtkm::cont::Field::Association::WHOLE_MESH:
        field = vtkm::cont::Field(name, assoc, vtkm::cont::make_ArrayHandle(buf, size));
        break;
      case vtkm::cont::Field::Association::CELL_SET:
        field = vtkm::cont::Field(name, assoc, assocLabel, vtkm::cont::make_ArrayHandle(buf, size));
        break;
      default:
        VTKM_ASSERT(false); // unsupported in vtkm
        break;
    }
  }
};

inline vtkm::cont::Field BuildField(const adios2::datamodel::Field &dmField,
                                    const ADIOS::VarInfo *var,
                                    void *buf)
{
  vtkm::cont::Field field;
  CastBufferAndCall(
    buf,
    var->GetType(),
    BuildFieldFunctor{},
    DimsSize(var->GetGlobalDims()),
    dmField.Name,
    FieldAssociationAdiosToVTKm(dmField.Association.AssociationType),
    dmField.Association.AssociationLabel,
    field);

  return field;
}

struct TransposeFieldArrayFunctor
{
  template <typename T>
  void operator()(const T* src, void* dstbuf, size_t major, size_t minor)
  {
    auto dst = static_cast<T*>(dstbuf);
    for (size_t j = 0; j < minor; ++j)
    {
      for (size_t i = 0; i < major; ++i)
      {
        dst[i*minor + j] = src[j*major + i];
      }
    }
  }
};

inline void TransposeFieldArray(
  std::vector<char>& buf, ADIOS_DATATYPES type, size_t major, size_t minor)
{
  std::vector<char> tmp(buf.size());
  CastBufferAndCall(buf.data(), type, TransposeFieldArrayFunctor{}, tmp.data(), major, minor);
  buf.swap(tmp);
}

const vtkm::cont::DataSet&
ADIOS1DataSetReader::ReadDataSet()
{
  for(auto &dmCellSet: this->Impl->DataModelDataSet.CellSets)
  {
    // Skip cell csets no longer in the read map
    if(this->Impl->VariableSources.count(&dmCellSet.Points) == 0 ||
       this->Impl->VariableSources.count(&dmCellSet.Cells) == 0)
    {
      continue;
    }

    //
    // TODO: Generalize for not the specific XGC case
    //
    assert(dmCellSet.Shape == adios2::datamodel::CellSetShape::Toroid);
    assert(dmCellSet.Points.Coordinates.Dims == adios2::datamodel::CoordinateSystemDimensions::TwoAndHalfD);
    assert(dmCellSet.Points.Coordinates.System == adios2::datamodel::CoordinateSystemType::MagneticRZT);
    assert(dmCellSet.Points.Coordinates.DimPlanar == "t");

    int varStep =
      dmCellSet.Source->Mode ==
      adios2::datamodel::DataSourceMode::MultiStep ?
      this->Impl->CurrentStep : 0;

    // First get the necessary planar info
    size_t numPlanes = 0;
    if(dmCellSet.Points.Coordinates.NumPlanesScalar.empty())
    {
      // Number of planes is hard coded to a fixed value
      numPlanes = dmCellSet.Points.Coordinates.NumPlanes;
    }
    else
    {
      // Number of planes needs to be read from a scalar variable
      for(auto scalarInfo:
          this->Impl->SourceReaders[
            this->Impl->VariableSources[&dmCellSet.Points].second]->GetScalars())
      {
        if(scalarInfo->GetName() == dmCellSet.Points.Coordinates.NumPlanesScalar)
        {
          numPlanes = scalarInfo->GetValue<int32_t>(varStep, 0);
        }
      }
    }

    // Now get array info
    auto &pointInfo = this->Impl->VariableInfo[&dmCellSet.Points];
    std::vector<size_t> pointDims;
    pointInfo->GetDims(pointDims, varStep, 0);

    // Allocate buffer data
    auto &pointBuf = this->Impl->ArrayBuffers[dmCellSet.Points.Array];
    pointBuf.resize(DimsSize(pointDims)*sizeof(double));

    // Now get cell info
    auto &cellInfo = this->Impl->VariableInfo[&dmCellSet.Cells];
    std::vector<size_t> cellDims;
    cellInfo->GetDims(cellDims, varStep, 0);

    // Allocate buffer data
    auto &cellBuf = this->Impl->ArrayBuffers[dmCellSet.Cells.Array];
    cellBuf.resize(DimsSize(cellDims)*sizeof(int32_t));

    // FIXME: create nextnode buffers
    auto &nnBuf = this->Impl->ArrayBuffers["nextnode"];
    nnBuf.resize(pointDims[0]*sizeof(int32_t));

    bool isCylindrical = !this->GenCartesian;
    bool isPeriodic = this->GenCartesian;

    // Build the actual arrays
    auto coords = vtkm::cont::make_ArrayHandleExtrudeCoords(
      reinterpret_cast<double*>(pointBuf.data()),
      DimsSize(pointDims), numPlanes, isCylindrical);

    auto cells = vtkm::cont::make_CellSetExtrude(
      vtkm::cont::make_ArrayHandle(reinterpret_cast<int32_t*>(cellBuf.data()), DimsSize(cellDims)),
      coords,
      vtkm::cont::make_ArrayHandle(reinterpret_cast<int32_t*>(nnBuf.data()), pointDims[0]),
      isPeriodic,
      dmCellSet.Name);

    this->Impl->Output.AddCellSet(cells);
    this->Impl->Output.AddCoordinateSystem(
      vtkm::cont::CoordinateSystem(dmCellSet.Name + "_coords", coords));

    // Schedule the array reads
    auto adiosPointReader =
      this->Impl->SourceReaders[
          this->Impl->VariableSources[&dmCellSet.Points].second];
    adiosPointReader->ScheduleReadArray(pointInfo->GetId(), pointBuf.data(),
      varStep, 0);

    // FIXME: schedule nextnode read
    auto a = adiosPointReader->GetArrays();
    auto nextnode = *std::find_if(
      a.begin(),
      a.end(),
      [](const typename decltype(a)::value_type& v){ return v->GetName() == "nextnode"; });
    adiosPointReader->ScheduleReadArray(nextnode->GetId(), nnBuf.data(), varStep, 0);

    auto adiosCellReader =
      this->Impl->SourceReaders[
          this->Impl->VariableSources[&dmCellSet.Cells].second];
    adiosCellReader->ScheduleReadArray(cellInfo->GetId(), cellBuf.data(),
      varStep, 0);

  }
  for(auto &dmField: this->Impl->DataModelDataSet.Fields)
  {
    // Skip fields no longer in the read map
    if(this->Impl->VariableSources.count(&dmField) == 0)
    {
      continue;
    }

    // Work out the appropriate read step
    int varStep =
      dmField.Source->Mode ==
      adios2::datamodel::DataSourceMode::MultiStep ?
      this->Impl->CurrentStep : 0;

    // First get array info
    auto &fieldInfo = this->Impl->VariableInfo[&dmField];
    const std::vector<size_t> &fieldDims = fieldInfo->GetGlobalDims();

    // Allocate buffer data
    auto &fieldBuf = this->Impl->ArrayBuffers[dmField.Array];
    fieldBuf.resize(
      DimsSize(fieldDims) * ADIOS::Type::SizeOf(fieldInfo->GetType()));

    // Schedule the array read
    auto adiosFieldReader =
      this->Impl->SourceReaders[
          this->Impl->VariableSources[&dmField].second];
    adiosFieldReader->ScheduleReadArray(fieldInfo->GetId(), fieldBuf.data(),
      varStep, 0);
  }

  // Process all scheduled reads
  for(auto &adiosReader : this->Impl->SourceReaders)
  {
    adiosReader.second->ReadArrays();
  }

  // Fix fields by transposing them
  for(auto &dmField: this->Impl->DataModelDataSet.Fields)
  {
    // Skip fields no longer in the read map
    if(this->Impl->VariableSources.count(&dmField) == 0)
    {
      continue;
    }

    // get array info
    auto &fieldInfo = this->Impl->VariableInfo[&dmField];
    const std::vector<size_t> &fieldDims = fieldInfo->GetGlobalDims();
    auto &fieldBuf = this->Impl->ArrayBuffers[dmField.Array];

    // transpose
    TransposeFieldArray(fieldBuf, fieldInfo->GetType(), fieldDims[1], fieldDims[0]);

    // Construct the field data
    vtkm::cont::Field field = BuildField(dmField, fieldInfo, fieldBuf.data());
    this->Impl->Output.AddField(field);
  }

  return this->Impl->Output;
}


int
ADIOS1DataSetReader::GetStep() const
{
  return this->Impl->CurrentStep;
}


bool
ADIOS1DataSetReader::Advance()
{
  // Initialize readers if necessary
  if(this->Impl->CurrentStep == -1)
  {
    this->Impl->InitializeADIOSReaders();

    for(auto &sourceReader : this->Impl->SourceReaders)
    {
      try
      {
        sourceReader.second->Open(sourceReader.first->Source);
      }
      catch(const ADIOS::ReadError)
      {
        return false;
      }
      if(sourceReader.first->Mode == adios2::datamodel::DataSourceMode::Series)
      {
        int step;
        std::sscanf(sourceReader.first->Source.c_str(),
          sourceReader.first->SeriesFormat.c_str(), &step);
        if(this->Impl->CurrentStep == -1)
        {
          this->Impl->CurrentStep = step;
        }
        else if(this->Impl->CurrentStep != step)
        {
          throw std::runtime_error(
            "Step mismatch from series reader " + sourceReader.first->Source);
        }
      }
    }
    if(this->Impl->CurrentStep == -1)
    {
      this->Impl->CurrentStep = 1;
    }
  }
  else
  {
    // Not our first step so let's kill off any read-once sources
    for(auto sourceReader = this->Impl->SourceReaders.cbegin();
        sourceReader != this->Impl->SourceReaders.cend(); )
    {
      if(sourceReader->first->Mode ==
         adios2::datamodel::DataSourceMode::SingleStep)
      {
        sourceReader->second->Close();
        this->Impl->SourceReaders.erase(sourceReader++);
      }
      else
      {
        ++sourceReader;
      }
    }

    // And let's do the same for read-once variables
    for(auto variableSource = this->Impl->VariableSources.cbegin();
         variableSource != this->Impl->VariableSources.cend(); )
    {
      if(variableSource->first->Static)
      {
        this->Impl->VariableInfo.erase(variableSource->first);
        this->Impl->VariableSources.erase(variableSource++);
      }
      else
      {
        ++variableSource;
      }
    }

    // What should be left now are multistep and series sources with non-static
    // variables

    // So let's now go through and open up the next series file for each series
    // reader
    for(auto &sourceReader : this->Impl->SourceReaders)
    {
      if(sourceReader.first->Mode == adios2::datamodel::DataSourceMode::Series)
      {
        char buf[256];
        std::memset(buf, 0, 256);
        std::sprintf(buf, sourceReader.first->SeriesFormat.c_str(),
          this->Impl->CurrentStep+1);
        sourceReader.second = std::make_shared<ADIOS::Reader>();
        try
        {
          sourceReader.second->Open(buf);
        }
        catch(const ADIOS::ReadError)
        {
          return false;
        }
      }
    }
    ++this->Impl->CurrentStep;
  }
  this->Impl->BuildVariableInfo();

  {
    // Save any static CellSets or Field data
    std::vector<vtkm::cont::DynamicCellSet> staticCellSets;
    std::vector<vtkm::cont::CoordinateSystem> staticCoordinateSystems;
    for(size_t i = 0; i < this->Impl->DataModelDataSet.CellSets.size(); ++i)
    {
      adios2::datamodel::CellSet &cs = this->Impl->DataModelDataSet.CellSets[i];
      if(this->Impl->Output.HasCellSet(cs.Name))
      {
        if(cs.Points.Static && cs.Cells.Static)
        {
          staticCellSets.push_back(this->Impl->Output.GetCellSet(cs.Name));
          staticCoordinateSystems.push_back(this->Impl->Output.GetCoordinateSystem(cs.Name + "_coords"));
        }
        else
        {
          this->Impl->ArrayBuffers.erase(cs.Points.Array);
          this->Impl->ArrayBuffers.erase(cs.Cells.Array);
        }
      }
    }
    std::vector<vtkm::cont::Field> staticFields;
    for(size_t i = 0; i < this->Impl->DataModelDataSet.Fields.size(); ++i)
    {
      adios2::datamodel::Field &f = this->Impl->DataModelDataSet.Fields[i];
      if(this->Impl->Output.HasField(f.Name))
      {
        if(f.Static)
        {
          staticFields.push_back(this->Impl->Output.GetField(f.Name));
        }
        else
        {
          this->Impl->ArrayBuffers.erase(f.Array);
        }
      }
    }

    // Clear the dataset for the next step
    this->Impl->Output.Clear();

    // Add back any static coords, cell set and field data
    for(auto &coords: staticCoordinateSystems)
    {
      this->Impl->Output.AddCoordinateSystem(coords);
    }
    for(auto &cs: staticCellSets)
    {
      this->Impl->Output.AddCellSet(cs);
    }
    for(auto &f: staticFields)
    {
      this->Impl->Output.AddField(f);
    }
  }

  return true;
}


} // end namespace reader
} // end namespace io
} // end namespace vtkm
