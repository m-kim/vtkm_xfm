#include <iostream>

#include <vtkmxfm/ADIOS1DataSetReader.h>

#include <vtkm/cont/DataSet.h>

int main(int argc, char **argv)
{
  if(argc != 3)
  {
    std::cerr << "Usage: /path/to/datamodel.json datamodel_type\n";
    return 1;
  }

  {
    vtkm::io::reader::ADIOS1DataSetReader reader(argv[1], argv[2], false);

    while(reader.Advance())
    {
      std::cout << "Reading step " << reader.GetStep() << "..." << std::flush;
      auto &dataset = reader.ReadDataSet();
      std::cout << " done" << std::endl;

      dataset.PrintSummary(std::cout);
    }
  }

  return 0;
}
