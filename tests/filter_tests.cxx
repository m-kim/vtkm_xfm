#include "vtkmxfm/vtkmxfm.h"

#include <vtkm/cont/DataSet.h>

#include <vtkm/filter/CellAverage.h>
#include <vtkm/filter/PointAverage.h>

#include <vtkm/cont/testing/Testing.h>

#include "tests/example_data.h"


int main(int, char**)
{
  const std::size_t numPlanes = 8;

  auto coords = vtkm::cont::make_ArrayHandleExtrudeCoords(points_rz, numPlanes, false);
  auto cells = vtkm::cont::make_CellSetExtrude(topology, coords, nextNode);

  vtkm::cont::DataSet dataset;

  dataset.AddCoordinateSystem(vtkm::cont::CoordinateSystem("coords", coords));
  dataset.AddCellSet(cells);

  // verify that a constant value point field can be accessed
  std::vector<float> pvalues(coords.GetNumberOfValues(), 42.0f);
  vtkm::cont::Field pfield(
    "pfield",
    vtkm::cont::Field::Association::POINTS,
    vtkm::cont::make_ArrayHandle(pvalues));
  dataset.AddField(pfield);

  // verify that a constant cell value can be accessed
  std::vector<float> cvalues(cells.GetNumberOfCells(), 42.0f);
  vtkm::cont::Field cfield(
    "cfield",
    vtkm::cont::Field::Association::CELL_SET,
    cells.GetName(),
    vtkm::cont::make_ArrayHandle(cvalues));
  dataset.AddField(cfield);

  vtkm::filter::PointAverage avg;
  try
  {
    avg.SetActiveField("cfield");
    auto result = avg.Execute(dataset, PolicyExtrude{});
    VTKM_TEST_ASSERT(result.HasField("cfield", vtkm::cont::Field::Association::POINTS), "filter resulting dataset should be valid");
  }
  catch(const vtkm::cont::Error&err)
  {
    std::cout << err.GetMessage() << std::endl;
    VTKM_TEST_ASSERT(false, "Filter execution threw an exception");
  }

  return 0;
}
